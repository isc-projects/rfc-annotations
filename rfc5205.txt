#A V. Risk, ISC.org
#C BIND 9 implementation note
#D 2022-08-15
#
This RFC is implemented in BIND 9.18 (all versions).
